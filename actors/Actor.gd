extends KinematicBody2D
class_name Actor

export var speed: = Vector2(300.0, 300.0)



var velocity: = Vector2.ZERO

# delta use to make game framerate independant 
func _physics_process(delta: float) -> void:
	move_and_slide(velocity)
	
