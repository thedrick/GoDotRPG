extends Actor





func _physics_process(delta: float) -> void:
	
	
	#Checks direction, if going left 0-1 = -1 if right 1-0 = 1
	var direction: = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"), 
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	)
	velocity = speed * direction
	
